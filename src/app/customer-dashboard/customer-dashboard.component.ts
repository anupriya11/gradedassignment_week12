import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup } from '@angular/forms';
import { customerModel } from './customer-dashboard board.model';
import { ApiService } from '../shared/api.service';


@Component({
  selector: 'app-customer-dashboard',
  templateUrl: './customer-dashboard.component.html',
  styleUrls: ['./customer-dashboard.component.css']
})
export class CustomerDashboardComponent implements OnInit {

  formValue !: FormGroup;
  customerModelObj : customerModel = new customerModel();
  customerData !: any;
  showAdd !: boolean;
  showUpdate !: boolean;
  constructor(private formbuilder : FormBuilder ,
    private api:ApiService) { }

  ngOnInit(): void {
    this.formValue = this.formbuilder.group({
      firstName : [''],
      lastName : [''],
      email : [''],
      phone : [''] 
    })
    this.getAllCustomer();
  }
  clickAddCustomer(){
    this.formValue.reset();
    this.showAdd = true;
    this.showUpdate = false;
  }
  postCustomerDetails(){
    this.customerModelObj.firstName = this.formValue.value.firstName ;
    this.customerModelObj.lastName = this.formValue.value.lastName;
    this.customerModelObj.email = this .formValue.value.email;
    this.customerModelObj.phone = this.formValue.value.phone;

    this.api.postCustomer(this.customerModelObj).subscribe(res=>{
      console.log(res);
      alert("user added sucessfully");
      let ref = document.getElementById('cancle');
      ref?.click();
      this.formValue.reset();
      this.getAllCustomer();
    },
    err=>{
      alert("something went wrong");
    })
    
  }
  
  getAllCustomer(){
    this.api.getCustomer().subscribe(res =>{
      this.customerData = res;
    })
  }
  deleteCustomer(row :any){
    this.api.deleteCustomer(row.id).subscribe(res =>{
      alert("User Deleted");
      this.getAllCustomer();
    })
  }
  onEdit(row : any){
    this.showUpdate = true;
    this.showAdd = false;
    this.customerModelObj.id = row.id;
    this.formValue.controls['firstName'].setValue(row.firstName);
    this.formValue.controls['lastName'].setValue(row.lastName);
    this.formValue.controls['email'].setValue(row.email);
    this.formValue.controls['phone'].setValue(row.phone);
        
  }
  updateCustomerDetails(){
    this.customerModelObj.firstName = this.formValue.value.firstName ;
    this.customerModelObj.lastName = this.formValue.value.lastName;
    this.customerModelObj.email = this .formValue.value.email;
    this.customerModelObj.phone = this.formValue.value.phone;
    this.api.updateCustomer(this.customerModelObj,this.customerModelObj.id).subscribe(res=>{
      alert("updated succesfully");
      let ref = document.getElementById('cancle');
      ref?.click();
      this.formValue.reset();
      this.getAllCustomer();
    })
  }
  
}
